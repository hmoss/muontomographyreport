\section{\label{sec:intro}{Introduction}}
Colombia features several currently active volcanoes within the Andes
mountain range that present a risk to the surrounding
population. Modelling the rock densities of the interior of these
volcanoes has been investigated previously by the MuTe collaboration~\cite{asorey2017muon} and
has the potential to inform geophysicists of the direction of travel
of volcano ejecta following eruption. 

Rock densities are calculated experimentally using the flux detected in a detector designed and
constructed by the collaboratxsion positioned at a
known site some distance from a candidate volcano. The hybrid detector
uses two movable $120~\textrm{cm} \times 120~\textrm{cm}$
scintillating panels separated by up to $200~\textrm{cm}$ and a
$120~\textrm{cm}^{3}$ water Cherenkov detector, providing a
coincidence measurement and measurement of muon energy. A full description of the detector can be
found within~\cite{sierra2018astroparticle}. The \CM volcano with
coordinates (4$\degree$29' N,75$\degree$22' W) was chosen from a list of candidates after evaluation
against a number of criteria~\cite{asorey2017muon} that determined the
efficacy of muon detection following passage through the volcano and
the safety of detector operation.

The work detailed in this report focuses on determining the viability
of the experimental technique through simulation. Collections of muons
are generated at a height consistent with the volcanic elevation
before their transport through a distance of rock consistent with \CM
and dependent on the muons position and angle within a local
coordinate system. Muon collections corresponding to 84 days were
generated, approximately the three month period of data collection
envisaged by the MuTe collaboration in order to acquire a
statistically significant number of muons and determine variations in
flux to $\mathcal{O}(10\%)$. 

\section{\label{sec:VolcanoTopo}Volcano Topography}
A potential detector position was
defined as 4.492298 N, 75.381092 W (equivalently 4° 29' 32.2728'' N,
75° 22' 51.9312'' W). This coordinate is used to define the position
of the detector throughout all following analysis and is shown in
relation to the volcanic dome in figure~\ref{fig:2DCoords}.

\begin{figure}[H]
       \centering
       \includegraphics[width=\textwidth]{figures/CoordMapContour}
 \caption[Top-down view of the topography of the volcanic
 area]{Top-down view of the topography of the \CM volcano and
   surrounding area. The position of the volcanic dome and the MuTe
   detector are highlighted. Elevation, in metres above sea level, is
   shown by the coloured bar. }
\label{fig:2DCoords}          
    \end{figure}

This work developed on the work of
D. Sierra-Porta who undertook preliminary investigations in this area,
in addition to A. Vesga Ram\'{i}rez~\cite{AlejaThesis} who provided the MATLAB-based code
used to determine the trajectories of muons passing through the
volcano to a pre-determined detector position and the associated
volcanic rock distance covered through the use of NASA Shuttle Radar
Topography Mission (SRTM) files. A representation of such trajectories
in relation to the volcanic topographical area is shown in figure \ref{fig:MuonTrajectories}.

\begin{figure}[H]
       \centering
       \includegraphics[width=\textwidth]{figures/3D_rays}
 \caption[Trajectories of muons passing through volcanic rock and
 entering the detector]{A representation of the possible trajectories
   of muons passing through $\geq~10~\textrm{m}$ of volcanic rock and
   entering the MuTe detector. Greater distances of traversed
   volcanic rock are depicted by red lines, while the lowest distances
 of traversed volcanic rock are shown by the blue lines. The elevation
of the surrounding topology is shown by the $z$-axis.}
\label{fig:MuonTrajectories}          
    \end{figure}

At the coordinate chosen for detector placement the relationship
between muon $\theta$ and $\phi$ and the amount of volcanic rock
traversed by the muon is shown in figure~\ref{fig:MuonDistanceVsAngle}.

\begin{figure}[H]
       \centering
       \includegraphics[width=\textwidth]{figures/pythonDistanceCode/MuonDistanceVsAngle.pdf}
 \caption[Distance through volcanic rock as a function of muon
 angle]{Distance through volcanic rock travelled by a muon
   as a function of muon $\theta$ and $\phi$.}
\label{fig:MuonDistanceVsAngle}          
    \end{figure}

\section{\label{sec:Generation}Generation of muon collection}
A collection of muons is generated through the use of a customised
version of the  Fortran-based
CORSIKA~\cite{heck1998corsika} extensive air shower simulation
software supplied by the LAGO collaboration. The LAGO collaboration
additionally supplied the ARTI analysis package that proved necessary
for the work in this report and enabled the generation of a collection
of secondary muons following the primary showering. The version of CORSIKA supplied by the LAGO
collaboration and the ARTI analysis software is available within
\url{https://github.com/lagoproject}. This version of CORSIKA uses the QGSJET-II-04\cite{QGSJet} high energy hadronic interaction model with
GHEISHA-2002~\cite{GHEISHA} used as the interaction model for low
energy particles. 

The version of CORSIKA uses in this case used as inputs the longitude and
latitude of the primary shower, the components of the magnetic field
of the Earth at this location, a relevant model of the atmosphere at
this location, angular and energy ranges for primary particles and the
required elevation at which secondary particles cease showering. The
number of showers to be generated was calculated proportionately to
the showering time required. In the case of all work that follows, 84
days of showering was simulated and the resulting muons stored. An
appropriate angular range was chosen for the zenith angle $\theta$,
defined as $0\degree$ at vertical and $90\degree$ when horizontal
(parallel to the surface of the Earth).

Following the collection of large samples of muons, two independent
procedures were followed to determine the amount of volcanic rock
through which muons were to be transported. Both procedures were informed by the output of the
MATLAB-based code produced by A. Vesga Ram\'{i}rez to select muons
lying within an appropriate angular range with the potential to pass
through a few metres of volcanic rock and be detected by the muon
telescope. The angular combinations and the corresponding distances of
volcanic rock traversed by a muon are shown in table~\ref{tab:MUSICInputParams}.

In the first case, muon $\phi$,
$x$, $y$ and $z$ coordinates were retained from the CORSIKA output,
with requirements placed such that $66\degree \leq \theta \leq 84\degree$ and
$117\degree \leq \phi \leq 147\degree$. The muon $\phi$ coordinate was
defined as $0\degree$ when pointing north in the $x$-$y$ plane. This
case is referred to as the `Fixed-$\phi$' case in the following.

A second prescription retained muon $\theta$ from CORSIKA while
randomising muon $\phi$ 100 times per muon. Following randomisation,
acceptable $\theta{},\phi$ combinations were assigned the
corresponding distance as calculated by the MATLAB code. The number of
randomisations per muon during data processing is stored and later used as a
normalisation factor. This case is referred to as the `Random-$\phi$' case.

A full description of the procedure used to generate a collection of
muons using CORSIKA is provided in appendix \ref{app:CORSIKA}.
\section{\label{sec:MUSIC}Muon transport with MUSIC}
Muons from CORSIKA possessing modified coordinates were transported
through a predetermined distance of material with the composition
of the volcanic rock of Cerro Mach\'{i}n using the Fortran-based \textbf{MU}on
\textbf{SI}mulation \textbf{C}ode (MUSIC)
software~\cite{Kudryavtsev:2008qh}. The composition of volcanic rock
was determined in previous unpublished work by D. Sierra. The volcano
rock density was treated as homogeneous in the first instance and was
set to $2.5~\textrm{g}~\textrm{cm}^{-3}$ in MUSIC. Key parameters in
MUSIC were set as defined in table~\ref{tab:MUSICParams} for the
default case.

\begin{table}[H]
\centering
\caption[MUSIC parameter settings]{MUSIC parameter settings}
\label{tab:MUSICParams}
\resizebox{\textwidth}{!}{
\begin{tabular}{@{}lr@{}}\toprule
Parameter & Value \\
\midrule
Radiation length & $25.89~\textrm{g}~\textrm{cm}^{-2}$ \\
Density & $2.5~\textrm{g}~\textrm{cm}^{-3}$\\
Transports per muon & 1\\
Multiple scattering & Off \\
\midrule
MUSIC Fortran arrays & \\
\midrule
Atomic numbers for all elements&[8.,11.,12.,13.,14.,15.,
        1    19.,20.,22.,25.,26.,9*0.]\\                                                                                          
        Atomic weights & [15.9994,22.98976,23.3050,26.98153,28.0855,30.97696,
        1    39.0983,40.078,47.867,54.93804,55.845,9*0]\\                                                                                        
        Elemental fraction by mass & [0.476,0.033,0.018,0.087,0.300,0.001,
        1    0.018,0.032,0.005,0.001,0.029,9*0.] \\                                                                                           
        Density correction parameters &
                                        [136.4,-3.774,0.083,3.412,3.055,0.049]
  \\ 

\bottomrule
\end{tabular}
}
\end{table}
This procedure was followed for both the fixed- and random-$\phi$
cases. An additional case is considered as an extension of the
random-$\phi$ case in which a region of the volcano is assigned a density of
$2.3~\textrm{g}~\textrm{cm}^{-3}$ in the region $127\degree \leq \phi
\leq 131\degree$ with all other areas of the volcano retaining a
density of $2.5~\textrm{g}~\textrm{cm}^{-3}$. Rock composition is held
constant. This case is referred to as the 'low-density region' case.

Due to the increased size of the dataset produced in the random-$\phi$
case, an additional post-processing step was introduced following muon
transport with MUSIC for the low-density region case. In all cases,
some additional post-processing was performed on the MUSIC
output, where output files were augmented with additional information from
input files. Information such as the initial coordinates and
$\theta,\phi$ values of muons were compared with values following muon
transport and used to confirm a muon trajectory consistent with
passage into the detector. 


\section{\label{sec:MUSIC}Results}
Analysis of the MUSIC output was performed for all cases detailed in
section \ref{sec:MUSIC}. The fixed-$\phi$ case was used as a
proof-of-concept to test the full analysis workflow and determine the
necessary number of muons that would need to be detected to determine
changes in volcanic rock density. The threshold number of muons
translates directly into the time it is necessary to station the
detector near the volcano, and it was hoped that results from the
following simulated tests could be used to inform the experimental team.

\subsection{\label{sec:FixedPhi}Fixed-$\phi$}
Results for the fixed-$\phi$ case were affected by an issue in which
smaller muon-rock distances were assigned to several points in
$\theta$ and $\phi$. This issue notwithstanding, the fixed-$\phi$ case
proved useful in gaining an understanding of the statistical size of
the eventual collected muon sample needed to deduce changes in rock
density.

The fixed-$\phi$ case was based on 84 days of CORSIKA shower
simulations which were subsequently transported through volcanic rock
in MUSIC as discussed in section \ref{sec:Generation}. The energy spectrum of the muons both prior to and following transport
through volcanic rock with MUSIC is shown in figure
\ref{fig:MuonInitialVFinalEnergy}. Generally, the number of muons at
each energy is reduced in the 'final' case, following transport
through an appropriate distance of volcanic rock. The feature present
below around $400~\textrm{MeV}$, in which there are more muons present
at low energies following MUSIC than prior to MUSIC, is expected as
the process of muon transport necessarily produces an abundance of low
energy muons.
\begin{figure}[H]
       \centering
       \includegraphics[width=\textwidth]{figures/FixedPhi/Apr2019/InitialEnergyVFinalEnergy_84Day.pdf}
 \caption[Energy profile of muons prior to and following transport
 through volcanic rock with MUSIC]{Energy in GeV of muons prior to and following transport through volcanic rock with MUSIC}
\label{fig:MuonInitialVFinalEnergy_Fixed}          
    \end{figure}

The distance-angle relationship for the fixed-$\phi$ case is shown in
figure \ref{fig:MuonAngleVDistance_Fixed} and includes erroneous
features discussed previously in this section. These are highlighted
by the presence of dark blue histogram bins present at lower values of
$\theta$ than expected. White histogram bins represent combinations of
$\theta$ and $\phi$ for which no muons were recorded. The
corresponding muon flux, as a function of $\theta$ and $\phi$, is
shown in figure \ref{fig:MuonNormFluxDay_Fixed}. In this case the flux
is normalised to show the number of muons entering each $0.5 \times
0.5$ degree bin per square centimetre, per steradian, per day.

\begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]{figures/FixedPhi/Apr2019/AngleVDistance_84Day.pdf}
 \caption[Distance-angle relationship for muons in the fixed-$\phi$
 case]{Two-dimensional histogram displaying the distance-angle relationship for muons in the fixed-$\phi$
   case. Bins coloured white are empty, and those coloured dark blue
   at low $\theta$ are affected by the issue discussed in section \ref{sec:FixedPhi}.}
\label{fig:MuonAngleVDistance_Fixed}          
    \end{figure}

\begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]{figures/FixedPhi/Apr2019/NormalisedAngularFlux_perDay_halfDegBins_relabelAnglesSR.pdf}
 \caption[Normalised muon flux per day as a function of muon
 angle]{Normalised muon flux per day as a function of muon
   angle. White histogram bins represent areas of $\theta$ and $\phi$
   where no muons were recorded.}
\label{fig:MuonNormFluxDay_Fixed}          
    \end{figure}

As not all angular combinations of interest were covered by the
fixed-$\phi$ scenario, alternative cases were explored. On analysing the muon flux following transport in the fixed-$\phi$
case and determining that a successful analysis framework was in
place, scenarios in which the $\phi$ coordinate of the muon was
randomised 100 times per muon to increase the size of the input to MUSIC.

\subsection{\label{sec:RandomPhiResults}Random-$\phi$}
The procedure for randomising the $\phi$ coordinate of muons prior to
propagation with MUSIC is described in section
\ref{sec:Generation} and was used to generate a dataset with reduced
statistical uncertainty.

Figures
\ref{fig:MuonAngleVDistance_RandomPhi} -
\ref{fig:NormalisedMuonFlux_perDay_halfDeg_RandomPhi} mirror those
produced for the fixed-$\phi$ case described in section
\ref{sec:FixedPhi}.

\begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]%{/Users/harry/MuonTomography/MUSIC_Analysis/AprRandomPhi_v3/v2/AngleVDistance_84Day.pdf}
{figures/RandomPhiPlots_UpdatedCMap/AngleVDistance_84Day.pdf}
 \caption[Distance-Angle relationship in the randomised-$\phi$ case]{Distance-Angle relationship in the randomised-$\phi$ case.}
\label{fig:MuonAngleVDistance_RandomPhi}          
    \end{figure}

\begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]%{/Users/harry/MuonTomography/MUSIC_Analysis/AprRandomPhi_v3/v2/NumberOfMuons_perAngle_halfDeg.pdf}
{figures/RandomPhiPlots_UpdatedCMap/NumberOfMuons_perAngle_halfDeg.pdf}
 \caption[Number of muons in each $0.5\times0.5$ degree bin]{Number of
 muons recorded in each $0.5 \times 0.5$ degree bin.}
\label{fig:MuonNumber_RandomPhi}
    \end{figure}

\begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]%{/Users/harry/MuonTomography/MUSIC_Analysis/AprRandomPhi_v3/v2/AngularFlux_perDay_halfDegree.pdf}
{figures/RandomPhiPlots_UpdatedCMap/AngularFlux_perSecond_OneDeg_logColorBar.pdf}
 \caption[Normalised muon flux as a function of angle for the
 randomised-$\phi$ case with $1\times{}1$ degree bins]{Normalised muon flux as a function of angle for the
 randomised-$\phi$ case with $1\times{}1$ degree bins}
\label{fig:NormalisedMuonFlux_perSec_OneDeg_RandomPhi}          
    \end{figure}

\begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]%{/Users/harry/MuonTomography/MUSIC_Analysis/AprRandomPhi_v3/v2/AngularFlux_perDay_halfDegree.pdf}
{figures/RandomPhiPlots_UpdatedCMap/AngularFlux_perDay_halfDegree.pdf}
 \caption[Normalised muon flux as a function of angle for the
 randomised-$\phi$ case with half-degree bins]{Normalised muon flux as a function of angle for the
 randomised-$\phi$ case with half-degree bins}
\label{fig:NormalisedMuonFlux_perDay_halfDeg_RandomPhi}          
    \end{figure}

Figures \ref{fig:MuonAngleVDistance_RandomPhi} -
\ref{fig:NormalisedMuonFlux_perDay_halfDeg_RandomPhi} show a combination of an increased size of the sample of
muons leading to reduced statistical uncertainty, while figure
\ref{fig:MuonAngleVDistance_RandomPhi} displays the
rectification of the issue systematically affecting all muons in the
fixed-$\phi$ case by unambiguously showing that correct muon-rock
distances are obtained for all $\theta$-$\phi$ combinations considered.

\subsubsection{Comparison with other sources}
Using this procedure, an increased statistical
sample of muons was generated and used to compare with a prior
study~\cite{asorey2017muon} and the master's thesis of A. Vesga Ram\'{i}rez~\cite{AlejaThesis}, which replaced the CORSIKA+MUSIC elements of the
method presented within this report with a mathematical model. 

A $1\times{}1$ degree bin in $(\theta{},\phi)$ was arbitrarily selected
from figure \ref{fig:MuonAngleVDistance_RandomPhi} with the criterion
that it was associated with a large muon-rock distance. This resulted
in the selection of the bin with $\theta=79.0$, $\phi=144.0$ and
distance $d=700~\textrm{m}$. Results were generated by transporting
through standard rock with a density of
$2.5~\textrm{g}\textrm{cm}^{-3}$, resulting in a
metres-of-water equivalent (MWE) distance traversed by the muons of
approximately $1750~\textrm{m}$. % reference for this conversion? 
The bin with $\theta = 70.0$, $\phi=129.0$ and distance
$d=197~\textrm{m} = 500~\textrm{m (MWE)}$ was also chosen for
comparison to determine the performance of CORSIKA+MUSIC at higher fluxes.

Flux values from CORSIKA+MUSIC (denoted $f_{\textrm{M}}$) were compared with equivalent values
from the work of A. Vesga Ram\'{i}rez~\cite{AlejaResults}, denoted
$f_{\textrm{A}}$. Additionally, vertical muon fluxes are provided in
the paper describing the MUSIC software~\cite{Kudryavtsev:2008qh} and
are generally expected to be greater than the fluxes obtained by the
simulation using CORSIKA+MUSIC in the range $66 \leq \theta \leq
84.0$. Vertical fluxes are referred to as $f_{\textrm{paper}}$ and are
given for particular distance values in MWE. A comparison of the flux
values obtained in both $(\theta,\phi)$ bins is shown in \ref{tab:FluxComparisons}.


% numbers need redoing!

\begin{table}[H]
\centering
\caption[Comparison of muon flux values across two
methodologies]{Comparison of muon flux values across difference
  methodologies. The random-$\phi$ case resulting from CORSIKA+MUSIC
  simulation is compared with the value obtained in the work of
  A. Vesga Ram\'{i}rez. Values of flux in in the vertical plane are
  provided from the paper detailing the MUSIC muon transport
  software~\cite{Kudryavtsev:2008qh}. All fluxes are shown in units of
$[\textrm{cm}^{-2}~\textrm{sr}^{-1}~\textrm{s}^{-1}]$.}
\label{tab:FluxComparisons}
\begin{tabular}{lllllll}\toprule
$\theta$ & $\phi$ &$d$ (MWE) &$f_{\textrm{M}}$& $f_{\textrm{A}}$ &
                                                                   $f_{\textrm{M}}/f_{\textrm{A}}$
                                                                   & $f_{\textrm{paper}}$ \\
\midrule
70.0 & 129.0 & 500.0 & $(1.145 \pm 0.007)\times{}10^{-5}$&
                                                        $9.21\times{}10^{-6}$&1.24&
                                                                               $1.06\times{}10^{-5}$\\
79.0 & 144.0 & 1750.0 & $(8.26 \pm  0.19)\times{}10^{-7}$&
                                                        $3.91\times{}10^{-7}$&2.11
                                                                   &---\\
90.0 & --- & 1000.0& ---&---&---&$1.47\times{}10^{-6}$\\
90.0 & --- & 2000.0& ---&---&---&$1.38\times{}10^{-7}$\\

\bottomrule
\end{tabular}
\end{table}

Values of $f_{\textrm{M}}/f_{\textrm{A}}$ were determined to be
acceptable if they were below $5.0$, although values up to $10.0$ were
tolerated. Values of this quantity approaching $1.0$ served to show
the accurate replication of the mathematical model used by A. Vesga
Ram\'{i}rez with CORSIKA+MUSIC, although it must be considered that
the CORSIKA+MUSIC simulation could potentially represent a more
accurate scenario. Figure \ref{fig:AlejandraFluxRatioComparison} shows
the quantity $f_{\textrm{M}}/f_{\textrm{A}}$ at all angles considered
by this analysis.

\begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]{figures/AlejandraComparison/AlejandraMUSICFlux_Ratio_goodCMap.pdf}
 \caption[Ratio of fluxes obtained from this and a previous study]{The
 ratio of muon fluxes from CORSIKA+MUSIC and a previous study by
 A. Vesga Ram\'{i}rez ($f_{\textrm{M}}/f_{\textrm{A}}$) encountered by a
detector situated at 4.492298 N, 75.381092 W.}
\label{fig:AlejandraFluxRatioComparison}          
    \end{figure}

Generally the flux expected from CORSIKA+MUSIC is higher than the
previous study. A notable exception to this is the region around $123
\leq \phi \leq 143$ and $84 \leq \theta \leq 80$, where CORSIKA+MUSIC
observed to predict a lower flux than the mathematical model for an
equivalent density and MWE distance of rock. The full set of muon counts with units [counts
$\textrm{cm}^{-2}~\textrm{s}^{-1}~\textrm{sr}^{-1}$] is given in
appendix \ref{sec:RandomFluxNormalisedCountsSec}.

With reasonable agreement between the two approaches, the
'low-density' scenario described in section \ref{sec:MUSIC} was investigated.

\subsection{\label{sec:LowDensityResults}Low-density region}
A final scenario largely mirrored the case described in
section~\ref{sec:RandomPhiResults}, with a muon collection generated
by CORSIKA and the $\phi$ coordinate randomised to reduce the
statistical error of the final estimate. Muons in the region $127.0
\leq \phi \leq 131.0$ were transported through a distance of rock
defined as in section \ref{sec:RandomPhiResults}, however a rock
density of $2.3~\textrm{g}~\textrm{cm}^{-3}$ was used during the MUSIC
muon transport. Muons outside of this $\phi$-range
were transported through rock with a density of
$2.3~\textrm{g}~\textrm{cm}^{-3}$ as in previous scenarios. Rock
composition remained unchanged.

Direct comparisons were drawn between this scenario and the
random-$\phi$ scenario described in
section~\ref{sec:RandomPhiResults}. Figure
\ref{fig:AngleFluxPerSecond_lowDensity} shows the normalised muon flux
as a function of angle for $1\times{}1$ degree bins and is comparible
to figure~\ref{fig:NormalisedMuonFlux_perSec_OneDeg_RandomPhi}.
\begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]{figures/LowDensityPlots/AngularFlux_perSecond_OneDeg_logColorBar.pdf}
 \caption[Normalised flux in $1\times{}1$ degree bins for the
 low-density region scenario]{Normalised flux per second in
   $1\times{}1$ degree bins for the low-density region scenario}
\label{fig:AngleFluxPerSecond_lowDensity}          
    \end{figure}
The low density region between $127.0
\leq \phi \leq 131.0$ is clearly observed at values of $\theta > 78$,
where fluxes are increased by one or two orders of magnitude.

The expected number of observed muons per 100 days is shown in figure~\ref{fig:NumberOfMuons100Days_lowDensity}.
\begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]{figures/LowDensityPlots/NumberOfMuons_perAngle_OneDeg_logColorBar_100Days.pdf}
 \caption[Number of muons expected after 100 days]{Number of muons
   expected in each
   $1\times{}1$ degree bin after 100 days of observation.}
\label{fig:NumberOfMuons100Days_lowDensity}          
    \end{figure}
Figure~\ref{fig:NumberOfMuons100Days_lowDensity} clearly shows an
increased number of muons observed originating from the low density
volcanic region, again with an increase of at least one order of
magnitude. The upper edge of the volcano is also clearly observed,
when muon fluxes rise as the rock distance traversed approaches
zero. Regions of the low density column near the volcanic dome are
not identifiable with respect to the rest of the volcano in
figure~\ref{fig:NumberOfMuons100Days_lowDensity}, leading to the
conclusion that such small changes in rock density may not be visible
in such areas.

\section{\label{sec:MUSIC}Conclusions}
This report used the simulated transport of a collection of muons through volcanic rock with the characteristics of
the active Cerro Mach\'{i}n volcano near Ibague, Colombia to determine the
efficacy of the eventual experimental implementation of this
study. Normalised muons fluxes and raw muons counts were determined
and will be used to inform the experimental strategy going forward, in
particular with regard to the placement of the MuTe detector and the
time of detector operation required. 

A low density region was artificially added to the muon transport
stage of this investigation to determine the potential sensitivity of
the detector to small changes in rock density. Regions of the volcano
featuring a lower rock density may represent favoured magma explusion
routes. Identification of such areas is therefore of increasing
interest to determine nearby populated areas at risk in the event of a
volcanic erruption. The small changes in rock density explored in
section~\ref{sec:LowDensityResults} are visible in lower elevation
areas of the volcano through variables such as normalised muon flux
and raw muon count, although this is not the case in higher elevation
areas near the volcanic dome where higher overall muon fluxes may
require a larger change in rock density in order for any lower density
features to be observed.




\section{\label{sec:Acknowledgements}Acknowledgements}
This research was made possible with a Global Challenge Research Fund
(GCRF) grant from the Science and Technology Funding Council
(STFC).

