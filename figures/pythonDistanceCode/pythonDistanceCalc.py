from __future__ import print_function
import os, time
import numpy as np
from pylab import *
from scipy import interpolate
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
from tqdm import tqdm



def distance_calc(X,Y,GDf3,longitude,latitude,mute1_vec,dome_vec,array,interpMethod,debug):
    # **Piece of code that calculates distances in rock**
    azi_i,azi_f,zen_i,zen_f=207,237,6,30
    azi_1=np.linspace(azi_i,azi_f,(azi_f-azi_i)*2.+1.)
    zen_1=np.linspace(zen_i,zen_f,(zen_f-zen_i)*2.+1.)
    polar=np.arange(azi_i,azi_f+0.5,0.5) # just mirroring the MATLAB code...
    polar_inv=polar-90
    zenith_inv=np.arange(zen_i,zen_f+0.5,0.5) # again mirrors MATLAB code
    zenith=90-zenith_inv
    conv=111.1949 #km por degree on GRID, Average radius of the earth: 6371km.
    conv=conv*1000 #in meters
    dx=3./3600*conv
    nh=2000
    npolar = len(polar)
    nzenith = len(zenith_inv)
    nlong  = npolar*nzenith
    h=1
    #Vector where the results will be registered
    results_vec=np.zeros((nlong,6))
    polar_med=((min(polar)+max(polar))/2)
    longitude_0=longitude-min(longitude)
    latitude_0=latitude-min(latitude)
    lon_mute1=mute1_vec[0]
    lat_mute1=mute1_vec[1]
    height_mute1=mute1_vec[2]
    
    
    getGX,getGY=np.meshgrid(longitude,latitude)
    
    getGX1=getGX[~array.mask]
    getGY1=getGY[~array.mask]
    newzt=array[~array.mask]
    
    G0=[lon_mute1,lat_mute1]
    if interpMethod=='cubic':
        zg=interpolate.griddata((getGX1, getGY1),newzt.ravel(),(G0[0], G0[1]),method='cubic')
    else:
        zg=interpolate.griddata((getGX1, getGY1),newzt.ravel(),(G0[0], G0[1]),method='linear')

    G0_0=[G0[0],G0[1],zg]

    P0_lon=G0[0]-min(longitude)
    P0_lat=G0[1]-min(latitude)
    P0_0=np.array([P0_lon,P0_lat,zg])
    P0_m=np.array([P0_lon*conv,P0_lat*conv,zg])
    lon_t_m=longitude_0*conv
    lat_t_m=latitude_0*conv
    lon_t_m_int=np.arange(min(lon_t_m),max(lon_t_m),dx/2)
    lat_t_m_int=np.arange(min(lat_t_m),max(lat_t_m),dx/2)
    XXint,YYint = np.meshgrid(lon_t_m_int,lat_t_m_int)
    xsmooth,ysmooth=np.meshgrid(lon_t_m,lat_t_m)
    xsmooth1=xsmooth[~array.mask]
    ysmooth1=ysmooth[~array.mask]

    if interpMethod=='cubic':
        zint_gd=interpolate.griddata((xsmooth1, ysmooth1),newzt.ravel(),(XXint, YYint),method='cubic')
    else:
        zint_gd=interpolate.griddata((xsmooth1, ysmooth1),newzt.ravel(),(XXint, YYint),method='linear')
    num=0
    counter=0
    print("Calculating muon distance travelled as a function of angle. Progress:")

    lon_dome=dome_vec[0]
    lat_dome=dome_vec[1]
    height_dome=dome_vec[2]


    # 3D surface plot - local coordinate format
    print ("Plotting 3D surface plot of the coordinate space in local coordinates:")
    plt.figure(figsize=(15,8))
    ax=plt.gca(projection='3d')
    print("shape of arrays:\n lon_t_m_int: {} \n lat_t_m_int: {} \n zint_gd: {}".format(lon_t_m_int.shape,lat_t_m_int.shape,zint_gd.shape))
    surf=ax.plot_surface(XXint,YYint,zint_gd,cmap='jet',linewidth=0.5,antialiased=True) # drawing the function
    #xpoints=[lon_mute1,lon_dome]
    #ypoints=[lat_mute1,lat_dome]
    #zpoints=[height_mute1,height_dome]
    #groupcolours=[1,2]
    #ax.scatter(xpoints,ypoints,zpoints,marker='o',c=groupcolours,s=64,depthshade=False)
    #ax.text(lon_mute1-0.001, lat_mute1+0.001,height_mute1,'Telescope')
    plt.colorbar(surf,shrink=0.7,aspect=15)
    ax.view_init(elev=60)
    plt.savefig('3DMap_localCoords.eps')
    plt.savefig('3DMap_localCoords.pdf')

    for k in tqdm(polar):
    #for k in polar:
        counter=counter+1
        for j in zenith_inv:
            krad=k*(math.pi/180)
            jrad=j*(math.pi/180)
            VV=[math.cos(krad),math.sin(krad),math.tan(jrad)]
            V_norm=np.sqrt(VV[0]**2+VV[1]**2+VV[2]**2)
            V=VV/V_norm
            length=0.
            length_of_nh=range(nh)
            P=0
            topo=0
            length_total=0
            for i in range(1,nh+1):
                P  = P0_m + i*h*V
                dx2=dx/2
                p0dx2=(P[0]/(dx/2))+1
                p1dx2=(P[1]/(dx/2))+1
                i_lat = math.floor(P[0]/(dx/2))+1
                i_lon = math.floor(P[1]/(dx/2))+1 # these should be ints!
                i_lat_integer=int(i_lat)
                i_lon_integer=int(i_lon)

                topo=zint_gd[i_lon_integer-1,i_lat_integer-1]
                
                if (topo>P[2]):
                    length=length+h
                    length_total=i

            polarmed_k_rad=(polar_med-k)*(math.pi/180)
            results_vec[num,0]=90-j # zenith angle
            results_vec[num,1]=k-90 # polar angle
            results_vec[num,2]=length # distance muons go into the volcano
            results_vec[num,3]=length_total*(math.cos(jrad))*(math.cos(polarmed_k_rad)) # x coordinate
            results_vec[num,4]=length_total*(math.cos(jrad))*(math.sin(polarmed_k_rad)) # y coordinate
            results_vec[num,5]=length_total*(math.sin(jrad))+P0_m[2] # z coordinate
            num=num+1
            

            if(k%1==0):
                x1,x2=P0_m[0],P[0]
                y1,y2=P0_m[1],P[1]
                z1,z2=P0_m[2],P[2]
                ax.plot([x1,x2],[y1,y2],[z1,z2],'g-')
                    
    np.savetxt('testMuonDistances.txt',results_vec,fmt='%.7e',delimiter='\t',newline='\n',header="Zenith [deg]\t Polar [deg] \t Distance [m] \t x [m] \t y [m] \t z [m]")
    angularDist=np.reshape(results_vec[:,[2]],(len(zenith_inv),len(polar)), order="F")
    flipangularDist=np.fliplr(angularDist)

    # debugging info:
    if debug is True:
        print("angularDist.shape: {}".format(angularDist.shape))
        print("angularDist: {}".format(angularDist))
        print("flipped angularDist: {}".format(flipangularDist))
        print("polar.shape: {} \n zenith.shape: {}".format(polar.shape,zenith.shape))
    

    print ("Plotting muon distance as a function of zenith (y-axis) and polar (x-axis) angles.")
    plt.figure(figsize=(8,7))
    ax=plt.gca()
#    im=ax.imshow(flipangularDist,extent=[min(polar), max(polar), min(zenith_inv), max(zenith_inv)],origin='lower',cmap='jet',aspect='auto',interpolation='none') # drawing the function
    im=ax.imshow(angularDist,extent=[min(polar), max(polar), max(zenith), min(zenith)],origin='lower',cmap='jet',aspect='auto',interpolation='none') # drawing the function
    plt.xlabel("phi [degrees]")
    plt.ylabel("theta [degrees]")
    # create an axes on the right side of ax. The width of cax will be 5%
    # of ax and the padding between cax and ax will be fixed at 0.05 inch.
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(im,cax=cax)
    plt.savefig('MuonDistanceVsAngle.eps')
    plt.savefig('MuonDistanceVsAngle.pdf')
    plt.show()

                    
    return None



