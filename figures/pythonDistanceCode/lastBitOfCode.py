
            if (mod(k, 1) == 0):        #& j==2)mod(j,2)==0)

            #line(mcat([P0_m[1], P[1]]), mcat([P0_m[2], P[2]]), mcat([P0_m[3], P[3]]), mstring('Marker'), mstring('.'), mstring('LineStyle'), mstring('-'), mstring('Color'), mcat([0, 0, 1]), mstring('LineWidth'), 1)

                fig=plt.figure(figsize=(15,8))
                X,Y = meshgrid(i_lon, i_lat) # grid of point
                Z = topo # evaluation of the function on the grid
                ax = fig.gca(projection='3d')
                surf = ax.plot_surface(X,Y,GDf3,cmap='jet',linewidth=0.5, antialiased=True)
                fig.colorbar(surf, shrink=0.7, aspect=15)
                ax.view_init(elev=60)#, azim=45)
                #colorbar(surf) # adding the colobar on the right
                plt.show()


    X = np.array([[1,1], [2,2.5], [3, 1], [8, 7.5], [7, 9], [9, 9]])
    Y = ['red', 'red', 'red', 'blue', 'blue', 'blue']

    plt.figure()
    plt.scatter(X[:, 0], X[:, 1], s = 170, color = Y[:])

    t1 = plt.Polygon(X[:3,:], color=Y[0])
    plt.gca().add_patch(t1)

    t2 = plt.Polygon(X[3:6,:], color=Y[3])
    plt.gca().add_patch(t2)

    plt.show()



    X[:3,:]


