#!/usr/bin/env python
import numpy as np
import struct
from pylab import *
from scipy import interpolate
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pyReadSRTM import get_elevation # function get_elevation is defined in pyReadSRTM.py

#mute1 is the location of the point 1 telescope at Machin
mute1_elev=get_elevation(-75.381092, 4.492298)
#define the dome of Machin
dome_elev=get_elevation(-75.390, 4.487)
print ("dome elevation: {}   mute elevation: {}".format(dome_elev,mute1_elev))
#Defining a array of longitude and latitude for observation points

#latitude=np.linspace(4.47,4.505,43) #both had 250 entries before
#longitude=np.linspace(-75.4,-75.37,37)
print("Defining latitude and longitude linspaces")

#latitude=np.linspace(4.47,4.505,10) #both had 250 entries before
#longitude=np.linspace(-75.4,-75.37,10)

latitudeOriginal=np.linspace(4.47,4.505,500)  
longitudeOriginal=np.linspace(-75.4,-75.37,500) # these two MUST be the same as the below line!!!!
longitude,latitude= np.mgrid[-75.4:-75.37:500j,4.47:4.505:500j]

x=longitude#.reshape((1201,1))
y=latitude#.reshape((1201,1))

########################################################################################################################
##### Do you only need to use meshgrid when the arrays are different sizes? It's apparently just a hangover from MATLAB
#### see: http://louistiao.me/posts/numpy-mgrid-vs-meshgrid/
########################################################################################################################

#Vectorize the function for calculating the array and make a grid with the 
#longitude and latitude defined above:

#print("Vectorizing get_elevation function")
f2 = np.vectorize(get_elevation)


print("longitude.shape: {} \n latitude.shape: {}".format(longitude.shape,latitude.shape))
print("longitude: {} \n latitude: {}".format(longitude,latitude))




#print("Defining meshgrid of long and lat")
#[X, Y] = np.meshgrid(longitude, latitude)
#X,Y=np.meshgrid(longitude,latitude,copy=False)

#print("X.shape: {} \n Y.shape: {}".format(X.shape,Y.shape))
#print("X: {} \n Y: {}".format(X,Y))



#f3=f2(X,Y)
f3=f2(longitude,latitude)
#f3=f2(x,y)
print("f3.size: {}".format(f3.size))
print("f3: {}".format(f3))


print("Vectorizing longitude and latitude")


#Mask invalid values. The data is filled with integer==-32768 for NaN values.
#These points are calculated using interpolation to mask the null values.

#z_t=f3


f3masked=np.where(f3==-32768,np.nan,f3)

array=np.ma.masked_invalid(f3masked)#np.ma.masked_invalid(array)
#print("Masked array: {}".format(array))

#xx,yy=np.mgrid(x, y)


#get only the valid values

#x1=xx[~array.mask]
#y1=yy[~array.mask]




x1=longitude[~array.mask] # are you masking the values of x and y that produce the masked values here?
y1=latitude[~array.mask]
newf3=array[~array.mask]

print("newf3.shape: {} \n x1.shape: {} \n y1.shape: {}".format(newf3.shape,x1.shape,y1.shape))

# from pyDistance:
#newf3.shape: (1509,) # 43 x 37 grid with masked values not considered
# x1.shape: (1509,) 
# y1.shape: (1509,) 
# xx.shape: (43, 37) 
# yy.shape: (43, 37)





#GDf3=interpolate.griddata((x1, y1),newf3.ravel(),(xx, yy),method='cubic')

#print("x1.shape: {} \n y1.shape: {} \n newf3.shape: {} \n longitude.shape: {} \n latitude.shape: {} \n xx.shape:{} \n yy.shape: {}".format(x1.shape,y1.shape,newf3.shape,longitude.shape,latitude.shape,xx.shape,yy.shape))

GDf3=interpolate.griddata((x1, y1),newf3.ravel(),(longitude, latitude),method='linear') # MATLAB code uses linear interpolation - try and optimise?

print("got GDf3! size: {} \n GDf3: {}".format(GDf3.size,GDf3))


# can plot the coordinate map here!
plt.figure(figsize=(8,7))
ax=plt.gca()
im=ax.imshow(GDf3,extent=[min(longitudeOriginal), max(longitudeOriginal), min(latitudeOriginal), max(latitudeOriginal)],origin='lower',cmap='jet',aspect='auto',interpolation='none') # drawing the function
plt.xlabel("theta [degrees]")
plt.ylabel("elevation [degrees]")
# create an axes on the right side of ax. The width of cax will be 5%
# of ax and the padding between cax and ax will be fixed at 0.05 inch.
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
#ticksx=np.arange(min(polar),max(polar)+1,5)
#ticksy=np.arange(min(zenith_inv),max(zenith_inv)+1,5)
#plt.xticks(ticksx)
#plt.yticks(ticksy)    
plt.colorbar(im,cax=cax)
plt.show()
    


# # **Calculate Distances**
# Establish a local coordinate system to do calculations. The new longitude and latitude in a system from the origin.
#Conversion factors to go from latitude and longitude to ground distances in a local coordinate system ...
fac1=111.1949*1000.
fac2=3./3600*fac1
#print fac2
def lat_n(x):
    return  (x-min(latitude))*fac1
def lon_n(x):
    return  (x-min(longitude))*fac1
#Convert from distances in land in local reference system to geographic coordinates
def lat_o(x):
    return  x/fac1+min(latitude)
def lon_o(x):
    return  x/fac1+min(longitude)

#latitude and longitude in metres
latitude_m=lat_n(latitude)
longitude_m=lon_n(longitude)



#Detector coordinates
lat_mute1=4.492298
lon_mute1=-75.381092
height_mute1=float(f2(lon_mute1,lat_mute1))
#print("height_mute1: {}".format(height_mute1))

mute1_vec=[lon_mute1,lat_mute1,height_mute1]
# Volcano dome coordinates
lat_dome=4.487
lon_dome=-75.390
height_dome=float(f2(-75.390, 4.487))
dome_vec=[lon_dome,lat_dome,height_dome]

#Defining azimuth and zenith to generate the grid spaced by 0.5 degrees
azi_i,azi_f,zen_i,zen_f=207,237,6,30
azi_1=np.linspace(azi_i,azi_f,(azi_f-azi_i)*2.+1.)
zen_1=np.linspace(zen_i,zen_f,(zen_f-zen_i)*2.+1.)



def distance_calc():
# **Piece of code that calculates distances in rock**
#    polar=np.arange(207,207+31,1)

    #polar=np.arange(azi_i,azi_f+1,0.5) # just mirroring the MATLAB code...
    polar=np.arange(azi_i,azi_f+0.5,0.5) # just mirroring the MATLAB code...
    polar_inv=polar-90
    #zenith_inv=np.arange(zen_i,zen_f+1,0.5) # again mirrors MATLAB code
    zenith_inv=np.arange(zen_i,zen_f+0.5,0.5) # again mirrors MATLAB code

    zenith=90-zenith_inv
    conv=111.1949 #km por degree on GRID, Average radius of the earth: 6371km.
    conv=conv*1000 #in meters
  #  print("conv: {}".format(conv))
    dx=3./3600*conv
  #  print("dx: {}".format(dx))
    nh=2000
    # nh=2000 in MATLAB code...
    #print polar
    npolar = len(polar)
    nzenith = len(zenith_inv)
    nlong  = npolar*nzenith

    h=1
    #print polar
    #print zenith_inv
    #print npolar, nzenith, nlong, nh, conv, dx

    #Vector where the results will be registered
    results_vec=np.zeros((nlong,6))
#    print("results_vec size: {}".format(results_vec.shape))
    
    polar_med=((min(polar)+max(polar))/2)

    

    #print long
    longitude_0=longitude-min(longitude)
    latitude_0=latitude-min(latitude)

    mute1_vec=[lon_mute1,lat_mute1,height_mute1]

    
    #altinterper=interpolate.interp2d(longitude,latitude,z_t,kind='cubic')
    getGX,getGY=np.meshgrid(longitude,latitude)
    
    getGX1=getGX[~array.mask]
    getGY1=getGY[~array.mask]
    newzt=array[~array.mask]


    
    #    G0_rbf = interpolate.Rbf(getGX, getGY, z_t, function='cubic', smooth=0)

    #G0_rbf = interpolate.Rbf(getGX1, getGY1, newzt, function='cubic', smooth=0)


    
    G0=[lon_mute1,lat_mute1]
 #   zg=interpolate.griddata((getGX1, getGY1),newzt.ravel(),(G0[0], G0[1]),method='cubic')
    zg=interpolate.griddata((getGX1, getGY1),newzt.ravel(),(G0[0], G0[1]),method='linear')




    #zg=altinterper(G0[0],G0[1])

    #zg=G0_rbf(G0[0],G0[1])

    G0_0=[G0[0],G0[1],zg]
    
    #P0_lon=lon_mute1-min(longitude)
    #P0_lat=lat_mute1-min(latitude)

    P0_lon=G0[0]-min(longitude)
    P0_lat=G0[1]-min(latitude)
    #P0_0=np.array([P0_lon,P0_lat,height_mute1])

    # just try setting zg to 2.4935*10^3
    #zg=2493.5
    # comment out zg if this breaks things
    P0_0=np.array([P0_lon,P0_lat,zg])
    P0_m=np.array([P0_lon*conv,P0_lat*conv,zg])
    
    #P0_m=np.array([-75.381092-min(longitude), 4.492298-min(latitude),2511])


    print ("P0_m.shape: {} \n P0_m: {}".format(P0_m.shape,P0_m))
    
    lon_t_m=longitude_0*conv
    lat_t_m=latitude_0*conv
    lon_t_m_int=np.arange(min(lon_t_m),max(lon_t_m),dx/2)
    lat_t_m_int=np.arange(min(lat_t_m),max(lat_t_m),dx/2)
        
    XXint,YYint = np.meshgrid(lon_t_m_int,lat_t_m_int)

    # Original 2D interpolation method from MATLAB: 
    #zint = interp2(lon_t_m,lat_t_m,z_t,XX,YY); # won't work - just copied from MATLAB
    
    # interp2d method:
    # need to provide 1D arrays to interper for some reason currently unknown to me!
    # see https://stackoverflow.com/questions/37872171/how-can-i-perform-two-dimensional-interpolation-using-scipy (which actually advises NOT to use interp2d...)


    #interper=interpolate.interp2d(lon_t_m,lat_t_m,z_t,kind='cubic')
    #xvec=XX[0,:] #1D array of the XX values
    #yvec=YY[:,0] #1D array of the YY values
    #zint = interper(xvec,yvec)
    #zint = double(zint)


    xsmooth,ysmooth=np.meshgrid(lon_t_m,lat_t_m)
    
#    print("shape of arrays \n lon_t_m.shape: {} \n lat_t_m.shape: {} \n z_t.shape: {}".format(xsmooth.shape,ysmooth.shape,z_t.shape))
  #  print("shape of arrays \n lon_t_m.shape: {} \n lat_t_m.shape: {} \n z_t.shape: {}".format(xsmooth.shape,ysmooth.shape,newzt.shape))


    xsmooth1=xsmooth[~array.mask]
    ysmooth1=ysmooth[~array.mask]

    
    #zint_gd=interpolate.griddata((xsmooth1, ysmooth1),newzt.ravel(),(XXint, YYint),method='cubic')
    zint_gd=interpolate.griddata((xsmooth1, ysmooth1),newzt.ravel(),(XXint, YYint),method='linear')

    #zfun_smooth_rbf = interpolate.Rbf(xsmooth, ysmooth, z_t, function='cubic', smooth=0)


    #zint_rbf=zfun_smooth_rbf(XX,YY)
    #zint_rbf=double(zint_rbf)

#    print("set up the rbf interpolation! \n shape of zint_gd: {}".format(zint_gd.shape))
    
    #Maybe actually better to use Rbf (radial basis functions) method
    # i.e.
    #interper=interpolate.Rbf(lon_t_m,lat_t_m,z_t,function='linear',smooth=0) # smooth defaults to 0 for interpolation
    #zint=interper(XX,YY) # can use the 2D arrays here...
    #zint=double(zint)

    #Another alternative is griddata:
    #    zint=interpolate.griddata(np.array([lon_t_m.ravel(),lat_t_m.ravel()]).T,
    #                              z_t.ravel(),
    #                              (XX,YY),method='linear')
    #    zint=double(zint)
    
    # Having problems with the alternatives so stick with interp2d for now...



    #"Position zero" (actually just the MuTe detector)
        
    num=0
    #print polar
    counter=0

    for k in polar:

        print ("polar angle step = {}/{}".format(counter+1,len(polar)))
        counter=counter+1
        for j in zenith_inv:

            krad=k*(math.pi/180)
            jrad=j*(math.pi/180)
            VV=[math.cos(krad),math.sin(krad),math.tan(jrad)]
            #VV=[math.cos(math.degrees(k)),math.sin(math.degrees(k)),math.tan(math.degrees(j))]  #doesn't do what you think it's doing!
           # if counter==1:
           #     print("k: {} \n j: {} \n VV: {}".format(k,j,VV))
                
                
            V_norm=np.sqrt(VV[0]**2+VV[1]**2+VV[2]**2)
            V=VV/V_norm
            length=0.
            length_of_nh=range(nh)
            P=0
            topo=0
            length_total=0
            for i in range(1,nh+1):
                P  = P0_m + i*h*V

                dx2=dx/2
                p0dx2=(P[0]/(dx/2))+1
                p1dx2=(P[1]/(dx/2))+1
                #print("P: {} \n P[0]: {} \n P[1]: {} \n dx/2: {} \n P[0]/(dx/2): {} \n P[1]/(dx/2): {}".format(P,P[0],P[1],dx2,p0dx2,p1dx2))

                
                i_lat = math.floor(P[0]/(dx/2))+1
                i_lon = math.floor(P[1]/(dx/2))+1 # these should be ints!

                i_lat_integer=int(i_lat)
                i_lon_integer=int(i_lon)
                
#                print("i_lat: {} i_lon: {}".format(i_lat_integer,i_lon_integer))

                
                #topo=f2(i_lon,i_lat) # this is not correct. need to use the interpolator
                #topo=f2(i_lon,i_lat)

                #topo=interper(i_lon,i_lat)
                topo=zint_gd[i_lon_integer-1,i_lat_integer-1]
               # print("topo: {}".format(topo))
                #topo=zfun_smooth_rbf(i_lon,i_lat)

                #topo=zfun_smooth_rbf(i_lon,i_lat)


                #topo=interpolate.griddata(np.array([xsmooth1.ravel(), ysmooth1.ravel()]).T,newzt.ravel(),(i_lon, i_lat),method='cubic')
                # don't need interpolation here!!!!!!!!!!!!

                


                #topo=double(topo)

                
                
                
#                ixx,iyy=np.meshgrid(i_lon, i_lat)

 #               ix1=ixx[~array.mask]
 #               iy1=iyy[~array.mask]
 #               inewf3=array[~array.mask]
 #               topo=interpolate.griddata((ix1, iy1),inewf3.ravel(),(ixx, iyy),method='cubic')

                
                #topo=interper(i_lon,i_lat) 


                if (topo>P[2]):
                    #print ("entry {} of {}: k={}, j={}, topo={}".format(str(i),nh,str(k),str(j),str(topo)))
                    length=length+h
                    length_total=i

            
            #print ("num = {}".format(num))
            polarmed_k_rad=(polar_med-k)*(math.pi/180)
            results_vec[num,0]=90-j # zenith angle
            results_vec[num,1]=k-90 # polar angle
            results_vec[num,2]=length # distance muons go into the volcano
            results_vec[num,3]=length_total*(math.cos(jrad))*(math.cos(polarmed_k_rad)) # x coordinate
            results_vec[num,4]=length_total*(math.cos(jrad))*(math.sin(polarmed_k_rad)) # y coordinate
            results_vec[num,5]=length_total*(math.sin(jrad))+P0_m[2] # z coordinate
            num=num+1

    #print("printing the results vector: {}".format(results_vec))
    np.savetxt('testMuonDistances.txt',results_vec,fmt='%.7e',delimiter='\t',newline='\n',header="Zenith [deg]\t Polar [deg] \t Distance [m] \t x [m] \t y [m] \t z [m]")
    print(results_vec[:,[2]])
    print("results_vec[:,2].shape: {}".format(results_vec[:,[2]].shape))
    angularDist=np.reshape(results_vec[:,[2]],(len(zenith_inv),len(polar)), order="F")
    flipangularDist=np.fliplr(angularDist)

    print("angularDist.shape: {}".format(angularDist.shape))
    print("angularDist: {}".format(angularDist))
    print("flipped angularDist: {}".format(flipangularDist))
    print("polar.shape: {} \n zenith.shape: {}".format(polar.shape,zenith.shape))




    
    plt.figure(figsize=(8,7))
    ax=plt.gca()
    
    
    im=ax.imshow(flipangularDist,extent=[min(polar), max(polar), min(zenith_inv), max(zenith_inv)],origin='lower',cmap='jet',interpolation='none') # drawing the function

    plt.xlabel("theta [degrees]")
    plt.ylabel("elevation [degrees]")

    # create an axes on the right side of ax. The width of cax will be 5%
    # of ax and the padding between cax and ax will be fixed at 0.05 inch.
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    
    #cset = plt.contour(polar,zenith_inv,flipangularDist,linewidths=1)#,colors='black',alpha=0.5)
    #plt.clabel(cset,inline=True,fontsize=12,fmt='%1.0f')
    # plt.imshow(flipangularDist,extent=[min(polar), max(polar), min(zenith_inv), max(zenith_inv)],origin='lower',cmap='jet',interpolation='hamming') # drawing the function


    #ticksx=np.arange(min(polar),max(polar)+1,5)
    #ticksy=np.arange(min(zenith_inv),max(zenith_inv)+1,5)

    #plt.xticks(ticksx,(210,215,220,225,230,235))

    #plt.xticks(ticksx)
    #plt.yticks(ticksy)

    plt.colorbar(im,cax=cax)
    #colorbar(im) # adding the colobar on the right
    plt.show()
    #if (mod(k, 1) == 0):        #& j==2)mod(j,2)==0)

            #line(mcat([P0_m[1], P[1]]), mcat([P0_m[2], P[2]]), mcat([P0_m[3], P[3]]), mstring('Marker'), mstring('.'), mstring('LineStyle'), mstring('-'), mstring('Color'), mcat([0, 0, 1]), mstring('LineWidth'), 1)

                #fig=plt.figure()
                #ax=fig.add_subplot(111,projection='3d')
                #ax.plot(([P0_m[1],P[1]]),([P0_m[2], P[2]]),([P0_m[3], P[3]]))
#    plt.show()

distance_calc()
