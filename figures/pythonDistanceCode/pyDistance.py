#!/usr/bin/env python
import numpy as np
import struct, sys
from pylab import *
from scipy import interpolate
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pyReadSRTM import get_elevation # function get_elevation is defined in pyReadSRTM.py
from pythonDistanceCalc import *


interpMethod = 'cubic'
debug=False

#mute1 is the location of the point 1 telescope at Machin
mute1_elev=get_elevation(-75.381092, 4.492298)
#define the dome of Machin
dome_elev=get_elevation(-75.390, 4.487)
print ("dome elevation: {}   mute elevation: {}".format(dome_elev,mute1_elev))
#Defining a array of longitude and latitude for observation points
# 43 and 37 are taken directly from the matlab code readhgt.m. When you start with 1201 samples (as for SRTM3) readhgt.m reduces to 43x37 before interpolation.
latitude=np.linspace(4.47,4.505,43) #both had 250 entries before in python implementation
longitude=np.linspace(-75.4,-75.37,37)

x=longitude
y=latitude

#Vectorize the function for calculating the array and make a grid with the 
#longitude and latitude defined above:
f2 = np.vectorize(get_elevation)

#Defining meshgrid of long and lat
[X, Y] = np.meshgrid(longitude, latitude)
f3=f2(X,Y)

#Mask invalid values. The data is filled with integer==-32768 for NaN values.
#These points are calculated using interpolation to mask the null values.

z_t=f3
f3masked=np.where(f3==-32768,np.nan,f3)

array=np.ma.masked_invalid(f3masked)

xx,yy=np.meshgrid(x, y)
#get only the valid values
x1=xx[~array.mask]
y1=yy[~array.mask]
newf3=array[~array.mask]

if interpMethod=='cubic':
    GDf3=interpolate.griddata((x1, y1),newf3.ravel(),(xx, yy),method='cubic')
else:
    GDf3=interpolate.griddata((x1, y1),newf3.ravel(),(xx, yy),method='linear') # MATLAB code uses linear interpolation - try and optimise?

# plot the coordinate map here!
print ("Plotting unlabelled coordinate map.")
plt.figure(figsize=(8,7))
ax=plt.gca()
im=ax.imshow(GDf3,extent=[min(longitude), max(longitude), min(latitude), max(latitude)],origin='lower',cmap='jet',aspect='auto',interpolation='none') # drawing the function
plt.xlabel("theta [degrees]")
plt.ylabel("elevation [degrees]")
# create an axes on the right side of ax. The width of cax will be 5%
# of ax and the padding between cax and ax will be fixed at 0.05 inch.
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
plt.colorbar(im,cax=cax)
plt.savefig('UnlabelledCoordMap.eps')
plt.savefig('UnlabelledCoordMap.pdf')
plt.show()


# # **Calculate Distances**
# Establish a local coordinate system to do calculations. The new longitude and latitude in a system from the origin.
#Conversion factors to go from latitude and longitude to ground distances in a local coordinate system ...
fac1=111.1949*1000.
fac2=3./3600*fac1
#print fac2
def lat_n(x):
    return  (x-min(latitude))*fac1
def lon_n(x):
    return  (x-min(longitude))*fac1
#Convert from distances in land in local reference system to geographic coordinates
def lat_o(x):
    return  x/fac1+min(latitude)
def lon_o(x):
    return  x/fac1+min(longitude)

#latitude and longitude in metres
latitude_m=lat_n(latitude)
longitude_m=lon_n(longitude)


#Detector coordinates
lat_mute1=4.492298
lon_mute1=-75.381092
height_mute1=float(f2(lon_mute1,lat_mute1))
#print("height_mute1: {}".format(height_mute1))

# mute coords in metres! # convention is lon is x, lat is y
lat_mute1_m=lat_n(lat_mute1)
lon_mute1_m=lon_n(lon_mute1)
print("(x,y,z) lon mute (m): {} \t lat mute (m): {} \t height mute (m): {}".format(lon_mute1_m,lat_mute1_m,height_mute1))


mute1_vec=[lon_mute1,lat_mute1,height_mute1]
# Volcano dome coordinates
lat_dome=4.487
lon_dome=-75.390
height_dome=float(f2(-75.390, 4.487))
dome_vec=[lon_dome,lat_dome,height_dome]


# 2D plot with contour lines + point of interest labels
print ("Plotting labelled coordinate map.")
plt.figure(figsize=(8,7))
ax=plt.gca()
im=ax.imshow(GDf3,extent=[min(longitude), max(longitude), min(latitude), max(latitude)],origin='lower',cmap='jet',aspect='auto',interpolation='none') # drawing the function
mute_point=plt.scatter(lon_mute1,lat_mute1)
plt.text(lon_mute1-0.001, lat_mute1+0.001, 'Telescope', fontsize=14, fontweight='bold')
dome_point=plt.scatter(lon_dome,lat_dome)
plt.text(lon_dome+0.001,lat_dome-0.001, 'Dome', fontsize=14, fontweight='bold')
cset = plt.contour(x,y,GDf3,linewidths=1)
plt.clabel(cset,inline=True,fontsize=12,fmt='%1.0f')
plt.colorbar(im)
plt.savefig('CoordMapContour.eps')
plt.savefig('CoordMapContour.pdf')
plt.show()


# 3D surface plot - lon/lat format
print ("Plotting 3D surface plot of the coordinate space.")
plt.figure(figsize=(15,8))
ax=plt.gca(projection='3d')
surf=ax.plot_surface(X,Y,GDf3,cmap='jet',linewidth=0.5,antialiased=True) # drawing the function
xpoints=[lon_mute1,lon_dome]
ypoints=[lat_mute1,lat_dome]
zpoints=[height_mute1,height_dome]
groupcolours=[1,2]
ax.scatter(xpoints,ypoints,zpoints,marker='o',c=groupcolours,s=64,depthshade=False)
ax.text(lon_mute1-0.001, lat_mute1+0.001,height_mute1,'Telescope')
plt.colorbar(surf,shrink=0.7,aspect=15)
ax.view_init(elev=60)
plt.savefig('3DMap.eps')
plt.savefig('3DMap.pdf')
plt.show()

distance_calc(X,Y,GDf3,longitude,latitude,mute1_vec,dome_vec,array,interpMethod,debug)
print ("Goodbye!")
