
# coding: utf-8

# In[1]:


#https://librenepal.com/article/reading-srtm-data-with-python/
#Import libraries
import numpy as np
import pylab
import struct


# In[2]:


#Make a function for read hgt file on topography and calculate altitudes for each point
SAMPLES = 1201 # Change this to 3601 for SRTM1
def read_elevation_from_file(hgt_file, lon, lat):
    with open(hgt_file, 'rb') as hgt_data:
        # Each data is 16bit signed integer(i2) - big endian(>)
        elevations = np.fromfile(hgt_data, np.dtype('>i2'), SAMPLES*SAMPLES)                                .reshape((SAMPLES, SAMPLES))
        lat_row = int(round((lat - int(lat)) * (SAMPLES - 1), 0))
        lon_row = int(round((lon - int(lon)) * (SAMPLES - 1), 0))
        return elevations[SAMPLES - 1 - lat_row, lon_row].astype(int)


# In[3]:


#A example
val11=read_elevation_from_file('N04W076.hgt',-75.389,4.489)
#mute1 is the location of the point 1 telescope in Machin
mute1=read_elevation_from_file('N04W076.hgt',-75.381092, 4.492298)
val11,mute1


# In[4]:


#Defining a array of longitude and latitude for observation points
latitude=np.linspace(4.47,4.505,250)
longitude=np.linspace(-75.4,-75.37,250)


# In[5]:


#for i in range(len(latitude)):
#    for j in range(len(longitude)):
#        result=read_elevation_from_file('N04W076.hgt',longitude[j],latitude[i])


# In[6]:


#Is needed vectorize the function for calculate in the array and make a grig with the 
#longitude and latitude defined above
f2 = np.vectorize(read_elevation_from_file)
[X, Y] = np.meshgrid(longitude, latitude)#, indexing = 'ij', sparse = 'true')


# In[ ]:


from scipy import interpolate
f3=f2('N04W076.hgt',X,Y)
#Mask the invalid values. The data in filled with number==-32768 when no exist data.
#This points are converted in NaN and then interpolate for fixed a point with a value data
f3=np.where(f3==-32768,np.nan,f3)


# In[ ]:


print f3[int((-75.381092-min(longitude))/0.00012)+1, int((4.492298-min(latitude))/0.00014)+1], f2('N04W076.hgt',-75.381092, 4.492298)
print f3[int((-75.389-min(longitude))/0.00012)+1, int((4.489-min(latitude))/0.00014)+1], f2('N04W076.hgt',-75.389, 4.489)


# In[ ]:


#masked_array = np.ma.array (a, mask=np.isnan(a))
#methods = [None, 'none', 'nearest', 'bilinear', 'bicubic', 'spline16',
#           'spline36', 'hanning', 'hamming', 'hermite', 'kaiser', 'quadric',
#           'catrom', 'gaussian', 'bessel', 'mitchell', 'sinc', 'lanczos']
plt.figure(figsize=(8,7))
X,Y = meshgrid(longitude, latitude) # grid of point
Z = f3 # evaluation of the function on the grid
#im = plt.contourf(X,Y,Z,cmap='jet')#,method='nearest') # drawing the function
im = imshow(Z,extent=[min(longitude), max(longitude), min(latitude), max(latitude)],origin='lower',
            cmap='jet',interpolation='hamming') # drawing the function
# adding the Contour lines with labels
punto=plt.scatter(-75.381092, 4.492298)
plt.text(-75.381092-0.001, 4.492298+0.001, 'Telescope', fontsize=14, fontweight='bold')
dome=plt.scatter(-75.390, 4.487)
plt.text(-75.390+0.001, 4.487-0.001, 'Dome', fontsize=14, fontweight='bold')
cset = plt.contour(X,Y,Z,linewidths=1)#,colors='black',alpha=0.5)
plt.clabel(cset,inline=True,fontsize=12,fmt='%1.0f')
colorbar(im) # adding the colobar on the right
# latex fashion title
#title('$z=(1-x^2+y^3) e^{-(x^2+y^2)/2}$')
plt.show()


# In[ ]:


#Masking the invalid values and interpolating over the existent values 
x=longitude
y=latitude
#mask invalid values
array=np.ma.masked_invalid(f3)#np.ma.masked_invalid(array)
xx,yy=np.meshgrid(x, y)
#get only the valid values
x1=xx[~array.mask]
y1=yy[~array.mask]
newf3=array[~array.mask]
GDf3=interpolate.griddata((x1, y1),newf3.ravel(),(xx, yy),method='cubic')
GDf3, f3[87-1,150-1], GDf3[87-1,150-1]


# In[ ]:


interpolate.griddata((x1, y1),newf3.ravel(),(-75.38, 4.48),method='cubic')


# In[ ]:


#masked_array = np.ma.array (a, mask=np.isnan(a))
#methods = [None, 'none', 'nearest', 'bilinear', 'bicubic', 'spline16',
#           'spline36', 'hanning', 'hamming', 'hermite', 'kaiser', 'quadric',
#           'catrom', 'gaussian', 'bessel', 'mitchell', 'sinc', 'lanczos']
plt.figure(figsize=(8,7))
X,Y = meshgrid(longitude, latitude) # grid of point
Z = f3 # evaluation of the function on the grid
#im = plt.contourf(X,Y,Z,cmap='jet')#,method='nearest') # drawing the function
im = imshow(GDf3,extent=[min(longitude), max(longitude), min(latitude), max(latitude)],origin='lower',
            cmap='jet',interpolation='hamming') # drawing the function
# adding the Contour lines with labels
punto=plt.scatter(-75.381092, 4.492298)
plt.text(-75.381092-0.001, 4.492298+0.001, 'Telescope', fontsize=14, fontweight='bold')
dome=plt.scatter(-75.390, 4.487)
plt.text(-75.390+0.001, 4.487-0.001, 'Dome', fontsize=14, fontweight='bold')
cset = plt.contour(X,Y,GDf3,linewidths=1)#,colors='black',alpha=0.5)
plt.clabel(cset,inline=True,fontsize=12,fmt='%1.0f')
colorbar(im) # adding the colobar on the right
# latex fashion title
#title('$z=(1-x^2+y^3) e^{-(x^2+y^2)/2}$')
plt.show()


# In[ ]:


from mpl_toolkits.mplot3d import Axes3D
fig=plt.figure(figsize=(15,8))
X,Y = meshgrid(longitude, latitude) # grid of point
Z = f3 # evaluation of the function on the grid
ax = fig.gca(projection='3d')
surf = ax.plot_surface(X,Y,GDf3,cmap='jet',linewidth=0.5, antialiased=True)
#punto=plt.scatter(-75.381092, 4.492298, 2400,c = 'b', marker='o')
#plt.text(-75.381092-0.001, 4.492298+0.001,'2500','Telescope', fontsize=14, fontweight='bold')
#dome=plt.scatter(-75.390, 4.487, 2750)
#ax.plot([-75.390 ,-75.381092],[4.487,4.492298],[2750,2400])
#plt.text(-75.390+0.001, 4.487-0.001, 'Dome', fontsize=14, fontweight='bold')
#plt.clabel(cset,inline=True,fontsize=12,fmt='%1.0f')
fig.colorbar(surf, shrink=0.7, aspect=15)
ax.view_init(elev=60)#, azim=45)
#colorbar(surf) # adding the colobar on the right
plt.show()


# # **Parte del código que calcula distancias**

# Vamos a establecer un sistema de coordenadas local para hacer cálculos. La nueva longitud y latitud en un sistema desde el origen.

# In[ ]:


#Convertir de latitud y longitud a distancias en suelo en el nuevo sistema local de coordenadas...
fac1=111.1929*1000.
fac2=3./3600*fac1
print fac2
def lat_n(x):
    return  (x-min(latitude))*fac1
def lon_n(x):
    return  (x-min(longitude))*fac1
#Convertir de distancias en suelo en sistema de referencia local a coordenadas geográficas
def lat_o(x):
    return  x/fac1+min(latitude)
def lon_o(x):
    return  x/fac1+min(longitude)

#latitud y longitud en metros
latitude_m=lat_n(latitude)
longitude_m=lon_n(longitude)

#Cooordenadas del detector
lat_mute1=4.492298
lon_mute1=-75.381092
height_mute1=float(f2('N04W076.hgt',lon_mute1,lat_mute1))
mute1_vec=[lon_mute1,lat_mute1,height_mute1]

#Definiendo azimuth y zenith para generar la grid espaciados por 0.5 grados
azi_i,azi_f,zen_i,zen_f=207,237,6,30
azi_1=np.linspace(azi_i,azi_f,(azi_f-azi_i)*2.+1.)
zen_1=np.linspace(zen_i,zen_f,(zen_f-zen_i)*2.+1.)

#Definiendo algunos puntos distantes del detector a una distancia de r=2000 metros medidos desde el suelo
r=2000.
lat_ran1=lat_n(lat_mute1)+r*np.cos(np.radians(azi_i))*np.cos(np.radians(0.))
lon_ran1=lon_n(lon_mute1)+r*np.sin(np.radians(azi_i))*np.cos(np.radians(0.))
lat_ran2=lat_n(lat_mute1)+r*np.cos(np.radians(azi_f))*np.cos(np.radians(0.))
lon_ran2=lon_n(lon_mute1)+r*np.sin(np.radians(azi_f))*np.cos(np.radians(0.))
lat_ran3=lat_n(lat_mute1)+r*np.cos(np.radians(222.))*np.cos(np.radians(0.))
lon_ran3=lon_n(lon_mute1)+r*np.sin(np.radians(222.))*np.cos(np.radians(0.))
height_ran3=float(f2('N04W076.hgt',lon_ran3,lat_ran3))

print mute1_vec
print lat_ran1, lon_ran1
print lat_ran2, lon_ran2
print lat_ran3, lon_ran3, lat_o(lat_ran3), lon_o(lon_ran3), height_ran3 


# In[ ]:


import matplotlib.patches as mpatches
plt.figure(figsize=(8,7))
X,Y = meshgrid(longitude_m, latitude_m) # grid of point
Z = f3 # evaluation of the function on the grid
#im = plt.contourf(X,Y,Z,cmap='jet')#,method='nearest') # drawing the function
im = imshow(GDf3,extent=[min(longitude_m), max(longitude_m), min(latitude_m), max(latitude_m)],
            origin='lower',cmap='jet',interpolation='hamming') # drawing the function

# adding the Contour lines with labels
punto=plt.scatter(lon_n(lon_mute1),lat_n(lat_mute1))
plt.text(lon_n(lon_mute1-0.001), lat_n(lat_mute1+0.001), 'Telescope', fontsize=14, fontweight='bold')

punto1=plt.scatter(lon_ran1,lat_ran1)
plt.text(lon_ran1-100., lat_ran1+100., 'P_ext1', fontsize=14, fontweight='bold')

punto2=plt.scatter(lon_ran2,lat_ran2)
plt.text(lon_ran2-100., lat_ran2+100., 'P_ext2', fontsize=14, fontweight='bold')

punto3=plt.scatter(lon_ran3,lat_ran3)
plt.text(lon_ran3-100., lat_ran3+100., 'P_test', fontsize=14, fontweight='bold')

#tri=[[lon_n(lon_mute1),lat_n(lat_mute1)],[lon_ran1,lat_ran1],[lon_ran2,lat_ran2]]
#triangle1=mpatches.Polygon(tri,fill=True,fc="blue",alpha=0.7)
#ax.add_artist(triangle1)

dome=plt.scatter(lon_n(-75.390), lat_n(4.487))
plt.text(lon_n(-75.390+0.001), lat_n(4.487-0.001), 'Dome', fontsize=14, fontweight='bold')

cset = plt.contour(X,Y,GDf3,linewidths=1)#,colors='black',alpha=0.5)
plt.clabel(cset,inline=True,fontsize=12,fmt='%1.0f')
colorbar(im) # adding the colobar on the right
# latex fashion title
#title('$z=(1-x^2+y^3) e^{-(x^2+y^2)/2}$')
plt.show()


# In[ ]:


#Vector donde se guaradarán los resultados
datos=np.zeros((len(azi_1)*len(zen_1),6))


# In[ ]:


#Por ejemplo haciendo una prueba
#Definiendo el vector unitario en la dirección del vector que se quiere propagar
phi_test,theta_test=222.,10.
Vx=float(np.sin(np.radians(phi_test))*np.cos(np.radians(theta_test)))
Vy=float(np.cos(np.radians(phi_test))*np.cos(np.radians(theta_test)))
Vz=float(np.sin(np.radians(theta_test)))

V_norm=math.sqrt(math.pow(Vx,2)+math.pow(Vy,2)+math.pow(Vz,2))
V_uni=np.array([Vx,Vy,Vz*111.1949*1000.])
#V_uni=np.array(V)/V_norm
print Vx, Vy, Vz, V_norm, V_uni
print lon_o(lon_ran3), lat_o(lat_ran3), height_ran3 


# In[ ]:


val1,val2=[],[]
nh=2000
len=0
for i in range(nh+1):
    #len=0.
    P=mute1_vec+i*1.*np.asarray(V_uni/111.1949/1000.)
    #print P
    topo=f2('N04W076.hgt',float((P[0])),float((P[1])))
    print topo, P[2]
    if (topo>P[2]):
        len=len+1
        len_total=i
        print topo, P[2], len, len_total 
        val1.append(len)
        val2.append(len_total)


# In[ ]:


val1[-1], val2[-1], val2[0], 'dist_total', val1[-1]+val2[0], 'dist_dentro', val2[-1]-val1[-1]


# In[ ]:


863+260, 1111-355


# In[ ]:


import matplotlib.patches as mpatches
plt.figure(figsize=(8,7))
X,Y = meshgrid(longitude_m, latitude_m) # grid of point
Z = f3 # evaluation of the function on the grid
#im = plt.contourf(X,Y,Z,cmap='jet')#,method='nearest') # drawing the function
im = imshow(GDf3,extent=[min(longitude_m), max(longitude_m), min(latitude_m), max(latitude_m)],
            origin='lower',cmap='jet',interpolation='hamming') # drawing the function

# adding the Contour lines with labels
punto=plt.scatter(lon_n(lon_mute1),lat_n(lat_mute1))
plt.text(lon_n(lon_mute1-0.001), lat_n(lat_mute1+0.001), 'Telescope', fontsize=14, fontweight='bold')

punto1=plt.scatter(lon_ran1,lat_ran1)
plt.text(lon_ran1-100., lat_ran1+100., 'P_ext1', fontsize=14, fontweight='bold')

punto2=plt.scatter(lon_ran2,lat_ran2)
plt.text(lon_ran2-100., lat_ran2+100., 'P_ext2', fontsize=14, fontweight='bold')

punto3=plt.scatter(lon_ran3,lat_ran3)
plt.text(lon_ran3-100., lat_ran3+100., 'P_test', fontsize=14, fontweight='bold')

punto=plt.scatter(lon_n(-75.38291745),lat_n(4.48871536))
plt.text(lon_n(-75.38291745)-100., lat_n(4.48871536)+100., 'in', fontsize=14, fontweight='bold')
punto=plt.scatter(lon_n(-75.38298982),lat_n(4.48857332))
plt.text(lon_n(-75.38298982)-100., lat_n(4.48857332)+100., 'out', fontsize=14, fontweight='bold')

dome=plt.scatter(lon_n(-75.390), lat_n(4.487))
plt.text(lon_n(-75.390+0.001), lat_n(4.487-0.001), 'Dome', fontsize=14, fontweight='bold')

cset = plt.contour(X,Y,GDf3,linewidths=1)#,colors='black',alpha=0.5)
plt.clabel(cset,inline=True,fontsize=12,fmt='%1.0f')
colorbar(im) # adding the colobar on the right
# latex fashion title
#title('$z=(1-x^2+y^3) e^{-(x^2+y^2)/2}$')
plt.show()


# **Pedazo de código que calcula distancias en roca**

# In[ ]:


polar=np.arange(207,207+31,1)
polar_inv=polar-90
cenit_inv=np.arange(6,6+25,1)
cenit=90-cenit_inv
conv=111.1949 #km por degree on GRID, Average radius of the earth: 6371km.
conv=conv*1000 #in meters
dx=3./3600*conv
nh=500
npolar = len(polar)
ncenit = len(cenit_inv)
nlong  = npolar*ncenit
h=1
print polar
print cenit_inv
print npolar, ncenit, nlong, nh, conv, dx


# In[ ]:


long=np.zeros((nlong,3))
print long
longitude_0=longitude-min(longitude)
latitude_0=latitude-min(latitude)
lon_t_m=longitude_0*conv
lat_t_m=latitude_0*conv
P0_m=np.array([-75.381092-min(longitude), 4.492298-min(latitude),2511])
print P0_m


# In[ ]:


k=polar[-1]
j=cenit_inv[-1]
VV=[math.cos(math.radians(k)),math.sin(math.radians(k)),math.tan(math.radians(j))]
V_norm=np.sqrt(VV[0]**2+VV[1]**2+VV[2]**2)
V=VV/V_norm
#print f3
len=0.
for i in range(nh):
    P  = P0_m + i*h*V
    #print P
    i_lat = floor(P[1]/(dx/2))+1.
    i_lon = floor(P[0]/(dx/2))+1.
    print (i_lat/2)
    topo=f2('N04W076.hgt',i_lon,i_lat)
    if (topo>P[2]):
        len=len+1.
                


# In[ ]:


num=0.
for k in polar:
    for j in cenit_inv:
        num=num+1.
        VV=[math.cos(math.radians(k)),math.sin(math.radians(k)),math.tan(math.radians(j))]
        V_norm=np.sqrt(VV[0]**2+VV[1]**2+VV[2]**2)
        V=VV/V_norm
        len=0.
        for i in range(nh):
            P  = P0_m + i*h*V
            i_lat = floor(P[1]/(dx/2))+1
            i_lon = floor(P[2]/(dx/2))+1
            topo=f2('N04W076.hgt',i_lon,i_lat)
            print topo
            if (topo>P[2]):
                len=len+1.
                
            
            
        


# In[ ]:


p,z=polar[-1],zenith[-1]
p_i,z_i=p-90,90-z
v=[math.cos(math.radians(p)),math.sin(math.radians(p)),math.tan(math.radians(z))]
v_norm=np.sqrt(v[0]**2+v[1]**2+v[2]**2)
v_u=v/v_norm
p,z


# In[ ]:


v, v_norm, v_u


# In[ ]:


longitude_0=longitude-min(longitude)
latitude_0=latitude-min(latitude)
lon_t_m=longitude_0*conv
lat_t_m=latitude_0*conv
P0_m=np.array([-75.381092-min(longitude), 4.492298-min(latitude),2511/conv])*conv


# In[ ]:


max(longitude_0), max(latitude_0)


# In[ ]:



#P0_m=[P0_lon, P0_lat, zg/conv]*conv
#lon_t_m


# In[ ]:


mute1_m


# In[ ]:


P=mute1_m+2000*v_u
P


# In[ ]:


i_lon=floor(P[1]/(dx/2))+1.
i_lat=floor(P[2]/(dx/2))+1.
i_lon, i_lat


# In[ ]:


len=0.
for i in range(2000):
    P=mute1_m+2000*v_u*i
    #print P
    i_lat = floor(P[1]/(dx/2))+1
    i_lon = floor(P[2]/(dx/2))+1
    topo=f2('N04W076.hgt',i_lon,i_lat)
    #print topo
    if (topo>P[2]):
        len=len+1.
        print len


# In[ ]:


topo


# In[ ]:


#% Calculating distances
num = 0
#polar = polar#mslice[dato(n, 3):1:dato(n, 4)]
#cenit_inv = zenith#mslice[dato(n, 7):1:30]
#npolar = len(polar)
#ncenit = len(cenit_inv)
nlong = 775.#npolar * ncenit
nh = 2000.#dato(n, 6)#Number of steps on the line
h = 1#Step size on the line...

long = np.zeros(775)#Arrangement with the distance traveled for each pair of angles

for k in polar:#For the polar angle measured from the x-axis
    for j in cenit_inv:    #para cenit inverso medido desde la horizontal (horizonte)
        num = num + 1    #contador para almacenar las longitudes recorridas..

        VV = mcat([cosd(k), sind(k), tand(j)])
        V = VV / norm(VV)    #Unit vector that determines the ray.

        len = 0
        clear(mstring('P'), mstring('topo'))
        for i in mslice[1:nh]:
            P = P0_m + i * h * V        #The many points on the straight
            i_lat = floor(P(1) / (dx / 2)) + 1
            i_lon = floor(P(2) / (dx / 2)) + 1
            topo = zint(i_lon, i_lat)
            if (topo > P(3)):            #If the topography is above the line...
                len = len + h            #The accumulated length traveled within the volcano...

            end
        end
        long(num, 1).lvalue = k    #The vector of lengths as a function of the polar angle and the inverse centit
        long(num, 2).lvalue = j
        long(num, 3).lvalue = len
        hold(mstring('on'))

        if (mod(k, 1) == 0):        #& j==2)mod(j,2)==0)
            hold(mstring('on'))
            line(mcat([P0_m(1), P(1)]), mcat([P0_m(2), P(2)]), mcat([P0_m(3), P(3)]), mstring('Marker'), mstring('.'), mstring('LineStyle'), mstring('-'), mstring('Color'), mcat([0, 0, 1]), mstring('LineWidth'), 1)

        end
    end

end


# In[ ]:


X = np.array([[1,1], [2,2.5], [3, 1], [8, 7.5], [7, 9], [9, 9]])
Y = ['red', 'red', 'red', 'blue', 'blue', 'blue']

plt.figure()
plt.scatter(X[:, 0], X[:, 1], s = 170, color = Y[:])

t1 = plt.Polygon(X[:3,:], color=Y[0])
plt.gca().add_patch(t1)

t2 = plt.Polygon(X[3:6,:], color=Y[3])
plt.gca().add_patch(t2)

plt.show()


# In[ ]:


X[:3,:]


# In[ ]:


tri=[[lon_n(lon_mute1),lat_n(lat_mute1)],[lon_ran1,lat_ran1],[lon_ran2,lat_ran2]]
tri

