#!/usr/bin/python                                                                          
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import math
from scipy.interpolate import griddata
import matplotlib.colorbar as cb
from matplotlib.colors import LinearSegmentedColormap
from pandas.tools.plotting import scatter_matrix
from scipy.interpolate import griddata
from scipy import interpolate
from scipy.interpolate import CubicSpline
from scipy.interpolate import interp1d
import os
from tqdm import tqdm
from physt import h1, h2, histogram, histogramdd
import dask
import dask.multiprocessing
import dask.array as da
from dask import dataframe as dd
from physt.compat.dask import h1 as d1
from physt.compat.dask import h2 as d2
import gc

def round_m(number):
    return np.round(number*2.)/2.
cm_data=np.loadtxt("/Users/harry/ColourMaps/lajolla/lajolla.txt")
CBname_map = LinearSegmentedColormap.from_list('CBname',cm_data)

test=np.loadtxt("allMuons_angular_OneDeg.txt")
thetaRange=np.arange(66.,85.) # hard code these while looping, need same bin widths for each file
phiRange=np.arange(117.,148.)
# need bins defined as left and right edges...

thetaBins=np.arange(66.,86.) # hard code these while looping, need same bin widths for each file
phiBins=np.arange(117.,149.)

allAnglesDF=pd.DataFrame(columns=['theta_i','phi_i'])
for theta in thetaRange: # sort allAnglesDF out outside the main loop
    for phi in phiRange:
        dfTemp=pd.DataFrame(np.array([[theta,phi]]),columns=['theta_i','phi_i'])
        allAnglesDF=allAnglesDF.append(dfTemp,ignore_index=True)

        
testFlat=test.flatten()
testDF=pd.DataFrame(testFlat,columns=['counts'])
bigDF=allAnglesDF
bigDF['counts']=testDF.counts
bigDF['std']=np.sqrt(bigDF.counts)




oneDegToRad=(np.pi)/180.

cosDiff=np.fabs(np.diff(np.cos(np.radians(thetaBins))))

SRConv=cosDiff*(oneDegToRad) # now an array!

norm_cms=float(1.0E4*84*24.0*3600.0*100.0)  # factor of 100 from randomising phi!
norm_day=float(1.0E4*84*100.0)  # factor of 100 from randomising phi!   

testRange=np.arange(66.,87.,1.)
bigDF.columns=['theta','phi','counts','sdev']


### draw raw number plots here ####
allAngleFlux, axedges_1Deg, ayedges_1Deg = np.histogram2d(bigDF.phi,bigDF.theta,bins=(phiBins,thetaBins))
allAngleFlux=allAngleFlux.T

### Number of muons as a function of angle (no normalisation)                              
plt.figure(figsize=(10,6))
ax=plt.gca()
allAngleFlux_1Deg_100Day=allAngleFlux/84
aAFHist1Deg=np.ma.masked_where(allAngleFlux_1Deg_100Day==0.0,allAngleFlux_1Deg_100Day)
im=plt.pcolormesh(phiRange,thetaRange,aAFHist1Deg,cmap=CBname_map,norm=matplotlib.colors.LogNorm())
ax.invert_yaxis()
plt.title(r'Normalised muon flux',fontsize=14)
plt.xlabel(r'$\phi$ [deg]',fontsize=14)
plt.ylabel(r'$\theta$ [deg]',fontsize=14)
divider=make_axes_locatable(ax)
cax=divider.append_axes("right",size='5%',pad=0.05)
cbar=plt.colorbar(im, cax=cax)
cbar.set_label("# muons",rotation=270,labelpad=15)
plt.tight_layout()
plt.savefig("test_plot_from_text_histo.pdf")



######



for entry in range(0,len(SRConv)):
    print(bigDF[bigDF.theta==testRange[entry]])
    bigDF.counts[bigDF.theta==testRange[entry]]/=SRConv[entry]
    bigDF.sdev[bigDF.theta==testRange[entry]]/=SRConv[entry]
    




    
norm_cms=float(1.0E4*84*24.0*3600.0*100.0)
bigDF.counts=bigDF.counts/norm_cms
bigDF.sdev=bigDF.sdev/norm_cms
#bigDF[bigDF.theta==79.0]
bigDF.to_csv("statsAnalysis_allAngles_OneDeg_Fixed.txt", sep='\t',index=False)
